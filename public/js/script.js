//import * as THREE from '../build/three.module.js';
//import Stats from './jsm/libs/stats.module.js';
//import { FirstPersonControls } from './jsm/controls/FirstPersonControls.js';

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshLambertMaterial( { color: 0x00ff00 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );
cube.castShadow = true; //default is false
cube.receiveShadow = true; //default

var geometry = new THREE.BoxGeometry( 2, 2 , 2 );
var material = new THREE.MeshLambertMaterial( { color: "red" } );
var cube2 = new THREE.Mesh( geometry, material );
scene.add( cube2 );
cube2.position.x = 3;


var geometry = new THREE.BoxGeometry( 1, 1 , 1 );
var material = new THREE.MeshLambertMaterial( { color: "pink", wireframe: false, opacity: 1, shininess: 30} );
var cubes = [];
for(let x = -25; x < 25; x++){
	for(let z = -25; z < 25; z++){
		cubes[cubes.length] = new THREE.Mesh( geometry, material );
		scene.add( cubes[cubes.length-1] );
		
		cubes[cubes.length-1].position.x = x*1;
		cubes[cubes.length-1].position.z = z*1;
		cubes[cubes.length-1].position.y = -2;
		if(Math.random() < 0.25){
			//material.color = "green";
			cubes[cubes.length] = new THREE.Mesh( geometry, material );
			scene.add( cubes[cubes.length-1] );
			
			cubes[cubes.length-1].position.x = x*1;
			cubes[cubes.length-1].position.z = z*1;
			cubes[cubes.length-1].position.y = -1;
		}
	}
}

// White directional light at half intensity shining from the top.
var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
scene.add( directionalLight );


var pointLight = new THREE.PointLight(0xffff00, 1000, 1, 2);
scene.add( pointLight );

//create a blue LineBasicMaterial
var material = new THREE.LineBasicMaterial( { color: 0x0000ff } );
var geometry = new THREE.Geometry();
geometry.vertices.push(new THREE.Vector3( -3, 0, 0) );
geometry.vertices.push(new THREE.Vector3( 0, 3, 0) );
geometry.vertices.push(new THREE.Vector3( 3, 0, 0) );
var line = new THREE.Line( geometry, material );
scene.add( line );






camera.position.z = 5;

var animate = function () {
	requestAnimationFrame( animate );

	cube.rotation.x += 0.01;
	cube.rotation.y += 0.01;

	cube2.rotation.x -= 0.01;
	cube2.rotation.y -= 0.01;

	camera.position.x += 0.01;
	camera.position.y += 0.01;

	renderer.render( scene, camera );
};

animate();